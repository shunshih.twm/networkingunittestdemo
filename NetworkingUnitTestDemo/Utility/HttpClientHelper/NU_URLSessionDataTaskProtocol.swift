//
//  NU_URLSessionDataTaskProtocol.swift
//  NetworkingUnitTestDemoTests
//
//  Created by Shun on 2018/10/22.
//  Copyright © 2018 com.shun.demo.NetworkingUnitTestDemo. All rights reserved.
//

import Foundation

protocol NU_URLSessionDataTaskProtocol {
    func resume()
}
