//
//  NU_URLSessionProtocol.swift
//  NetworkingUnitTestDemoTests
//
//  Created by Shun on 2018/10/22.
//  Copyright © 2018 com.shun.demo.NetworkingUnitTestDemo. All rights reserved.
//

import UIKit

protocol NU_URLSessionProtocol {
    typealias DataTaskResult = (Data?, URLResponse?, Error?) -> Void
    
    func dataTask(with request: NSURLRequest, completionHandler: @escaping DataTaskResult) -> NU_URLSessionDataTaskProtocol
}
