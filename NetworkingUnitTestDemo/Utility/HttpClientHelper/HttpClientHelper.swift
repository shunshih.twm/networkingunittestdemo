//
//  HttpClientHelper.swift
//  NetworkingUnitTestDemo
//
//  Created by Shun on 2018/10/22.
//  Copyright © 2018 com.shun.demo.NetworkingUnitTestDemo. All rights reserved.
//

import UIKit

enum HttpMethodEnum: String{
    case GET = "GET"
    case POST = "POST"
}

class HttpClientHelper {
    // MARK: - Type Alias
    typealias completeClosure = ( _ data: Data?, _ error: Error?)->Void
    
    // MARK: - Property
    private let session: NU_URLSessionProtocol
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    init(session: NU_URLSessionProtocol) {
        self.session = session
    }
    // MARK: - Life Cycle
    deinit {
        print("shun - \( type(of: self) ) deinit")
    }
    
    // MARK: - Private Func
    
    // MARK: - Public Func
    func get(url: URL, completion: @escaping completeClosure){
        let request = NSMutableURLRequest.init(url: url)
        request.httpMethod = HttpMethodEnum.GET.rawValue
      
        let task = self.session.dataTask(with: request) { (data, response, error) in
            completion(data,error)
        }
        task.resume()
    }

}
