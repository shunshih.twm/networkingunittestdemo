//
//  URLSession+dataTask.swift
//  NetworkingUnitTestDemo
//
//  Created by Shun on 2018/10/22.
//  Copyright © 2018 com.shun.demo.NetworkingUnitTestDemo. All rights reserved.
//

import UIKit

extension URLSession: NU_URLSessionProtocol {
    func dataTask(with request: NSURLRequest, completionHandler: @escaping URLSession.DataTaskResult) -> NU_URLSessionDataTaskProtocol {
        return dataTask(with: request as URLRequest, completionHandler: completionHandler) as NU_URLSessionDataTaskProtocol
    }
    
}
