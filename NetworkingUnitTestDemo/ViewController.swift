//
//  ViewController.swift
//  NetworkingUnitTestDemo
//
//  Created by Shun on 2018/10/22.
//  Copyright © 2018 com.shun.demo.NetworkingUnitTestDemo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let apiUrl = "https://jsonplaceholder.typicode.com/posts/42"
        let helper = HttpClientHelper.init(session: URLSession.shared)
        guard let url = URL.init(string: apiUrl)  else {
            fatalError("URL is nil")
            //return
        }
        
        helper.get(url: url) { (data, error) in
            print("\(String(data: data!, encoding: String.Encoding.utf8) ?? "")")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

