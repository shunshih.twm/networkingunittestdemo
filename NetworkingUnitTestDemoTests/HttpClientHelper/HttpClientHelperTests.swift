//
//  HttpClientHelperTests.swift
//  NetworkingUnitTestDemoTests
//
//  Created by Shun on 2018/10/22.
//  Copyright © 2018 com.shun.demo.NetworkingUnitTestDemo. All rights reserved.
//

import XCTest
@testable import NetworkingUnitTestDemo

class HttpClientHelperTests: XCTestCase {
    
    var httpClientHelper: HttpClientHelper!
    let session = MockURLSession()
    
    override func setUp() {
        super.setUp()
        
        self.httpClientHelper = HttpClientHelper.init(session: self.session)
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_get_request_with_URL() {
        
        guard let url = URL(string: "https://mockurl") else {
            fatalError("URL can't be empty")
        }
        
        httpClientHelper.get(url: url) { (success, response) in
            // Return data
        }
        
        XCTAssert(session.lastURL == url)
        
    }
    
    func test_get_resume_called() {
    
        let dataTask = MockURLSessionDataTask()
        self.session.mockDataTask = dataTask
        
        guard let url = URL(string: "https://mockurl") else {
            fatalError("URL can't be empty")
        }
        
        httpClientHelper.get(url: url) { (success, response) in
            // Return data
        }
        
        XCTAssert(dataTask.resumeWasCalled)
    }
    
    func test_get_should_return_data() {
        let expectedData = "{}".data(using: .utf8)
        
        self.session.mockData = expectedData
        
        guard let url = URL(string: "https://mockurl") else {
            fatalError("URL can't be empty")
        }
        
        var actualData: Data?
        httpClientHelper.get(url: url) { (data, error) in
            actualData = data
        }
        
        XCTAssertNotNil(actualData)
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
