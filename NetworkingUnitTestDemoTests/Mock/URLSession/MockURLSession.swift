//
//  MockURLSession.swift
//  NetworkingUnitTestDemoTests
//
//  Created by Shun on 2018/10/22.
//  Copyright © 2018 com.shun.demo.NetworkingUnitTestDemo. All rights reserved.
//

import UIKit
@testable import NetworkingUnitTestDemo

class MockURLSession: NU_URLSessionProtocol {

    var mockDataTask: NU_URLSessionDataTaskProtocol
    var mockData: Data?
    private var mockError: Error?
    
    private (set) var lastURL: URL?
    
    init(dataTask: NU_URLSessionDataTaskProtocol = MockURLSessionDataTask()) {
        self.mockDataTask = dataTask
    }
    
    func dataTask(with request: NSURLRequest, completionHandler: @escaping MockURLSession.DataTaskResult) -> NU_URLSessionDataTaskProtocol {
        self.lastURL = request.url
        
        completionHandler(mockData, successHttpURLResponse(request: request), mockError)
        return mockDataTask
    }
    
    func successHttpURLResponse(request: NSURLRequest) -> URLResponse {
        return HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: "HTTP/1.1", headerFields: nil)!
    }
}
