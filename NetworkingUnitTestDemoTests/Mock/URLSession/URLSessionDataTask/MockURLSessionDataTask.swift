//
//  MockURLSessionDataTask.swift
//  NetworkingUnitTestDemoTests
//
//  Created by Shun on 2018/10/22.
//  Copyright © 2018 com.shun.demo.NetworkingUnitTestDemo. All rights reserved.
//

import UIKit
@testable import NetworkingUnitTestDemo

class MockURLSessionDataTask: NU_URLSessionDataTaskProtocol{
    private (set) var resumeWasCalled = false
    
    func resume() {
        self.resumeWasCalled = true
    }
}
